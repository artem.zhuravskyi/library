package com.example.demo.service;

import com.example.demo.dao.OrderRepository;
import com.example.demo.model.Order;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @Mock
    private OrderRepository testOrderRepository;
    @InjectMocks
    private OrderService orderService;


    @Test
    void orderInfo() {
        User user = new User();
        user.setRole(Role.ROLE_USER);
        user.setId(1L);
        Order order = Order.builder()
                .user(user)
                .status(Order.Status.REQUESTED)
                .build();
        List<Order> allFoundOrders = List.of(order);
        Mockito.when(testOrderRepository.findOrderByUserAndStatus(any(),any())).thenReturn(allFoundOrders);

        List<Order> result = orderService.orderInfo(user);

        assertEquals(allFoundOrders, result);
        user.setRole(Role.ROLE_ADMIN);
        result = orderService.orderInfo(user);
        assertNull(result);
        user.setRole(Role.ROLE_LIBRARIAN);
        result = orderService.orderInfo(user);
        assertEquals(allFoundOrders, result);
    }
}