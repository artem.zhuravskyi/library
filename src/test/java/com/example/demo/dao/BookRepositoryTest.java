package com.example.demo.dao;

import com.example.demo.model.Book;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class BookRepositoryTest {

    @Autowired
    private BookRepository underTest;

    @Test
    void itShouldFindBookByAuthor() {
        Book book = new Book(
            1L,
            12,
            "Title",
            "Author",
            "Publisher",
            LocalDate.now());

        underTest.save(book);
        String expected = underTest.findBookByAuthor(book.getAuthor()).get().getAuthor();
        assertEquals(expected, "Author");

    }

    @Test
    void findByTitleContainsIgnoreCase() {
    }

    @Test
    void findByAuthorContainsIgnoreCase() {
    }

    @Test
    void findBookByTitle() {
    }

    @Test
    void findBookByAuthorAndTitleAndPublisher() {
    }
}