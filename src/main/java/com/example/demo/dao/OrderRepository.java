package com.example.demo.dao;

import com.example.demo.model.Book;
import com.example.demo.model.Order;
import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findOrderByUser(User user);

    List<Order> findOrderByUserAndStatus(User user, Order.Status status);

    List<Order> findByStateIs(Order.State state);

    List<Order> findOrderByStatus(Order.Status requested);


    void deleteByIdIn(List<Long> ids);
    List<Order> findOrderIdByBook(Long id);

    void deleteAllByBookAndStatus(Book book, Order.Status status);
}
