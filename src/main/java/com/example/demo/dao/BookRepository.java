package com.example.demo.dao;

import com.example.demo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findBookByAuthor(String author);

    List<Book> findByTitleContainsIgnoreCase(String title);
    List<Book> findByAuthorContainsIgnoreCase(String title);

    Optional<Book> findBookByTitle(String title);
    Optional<Book> findBookByAuthorAndTitleAndPublisher(String author, String title, String publisher);

}
