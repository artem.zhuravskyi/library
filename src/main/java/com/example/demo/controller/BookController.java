package com.example.demo.controller;

import com.example.demo.dto.BookDTO;
import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class BookController {

    private final BookService bookService;


    @PostMapping("/addBook")
    public String addBook(@ModelAttribute("bookDTO") BookDTO bookDTO) {
        bookService.addBook(bookDTO);
        return "redirect:/userPage";
    }

    @GetMapping("/addNewBook")
    public String addNewBook() {
        return "addNewBook";
    }

    @PatchMapping("/returnBook/{id}")
    public String returnBook(@PathVariable("id") Long id) {
        bookService.returnBook(id);
        return "redirect:/userPage";
    }

    @GetMapping("/editPageBook/{id}")
    public String editPageBook(@PathVariable Long id,
                               Model model) {
        model.addAttribute("Book", bookService.findProperBook(id));
        return "editPageBook";
    }

    @PatchMapping("/editBook/{id}")
    public String editBook(@PathVariable("id") Long id,
                           BookDTO bookDTO) {
        bookService.editBook(id, bookDTO);
        return "userPage";
    }

    @DeleteMapping("/deleteBook/{id}")
    public String deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return "redirect:/userPage";
    }
}
