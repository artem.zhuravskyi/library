package com.example.demo.controller;

import com.example.demo.dao.UserRepository;
import com.example.demo.dto.OrderDTO;
import com.example.demo.model.Book;
import com.example.demo.model.Order;
import com.example.demo.service.BookService;
import com.example.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Controller
public class UserController {

    private final BookService bookService;
    private final UserService userService;

    @GetMapping("/searchByTitle")
    public String searchByTitle(Model model,
                                @RequestParam("searchContent") String searchContent) {
        model.addAttribute("books", bookService.searchByTitle(searchContent));
        model.addAttribute("order", new Order());
        return "searchList";
    }

    @GetMapping("/searchByAuthor")
    public String searchByAuthor(Model model,
                                @RequestParam("searchContent") String searchContent) {
        model.addAttribute("books", bookService.searchByAuthor(searchContent));
        model.addAttribute("orderDTO", new OrderDTO());
        return "searchList";
    }

    @DeleteMapping("/deleteLibrarian/{id}")
    public String deleteLibrarian(@PathVariable("id") Long id) {
        userService.deleteLibrarian(id);
        return "redirect:/userPage";
    }

}
