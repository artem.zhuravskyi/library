package com.example.demo.controller;

import com.example.demo.dto.OrderDTO;
import com.example.demo.model.Book;
import com.example.demo.model.Order;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Controller
@Log4j2
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/userPage")
    public String orderInfo(Model model,
                            Authentication authentication,
                            @RequestParam("sortBy") Optional<String> sortBy,
                            @RequestParam("direction") Optional<String> direction,
                            @RequestParam("page") Optional<Integer> pageNo) {

        User current = (User) authentication.getPrincipal();
        log.info("LOGINFO1");
        model.addAttribute("orderInfo", orderService.orderInfo(current));
        int currentPage = pageNo.orElse(1);
        String sort = sortBy.orElse("id");
        String dir = "asc".equalsIgnoreCase(direction.orElse("asc")) ? "asc" : "desc";

        log.info("LOGINFO2");
        Page<Order> page = orderService.getPaginated(currentPage, sort, dir);
        List<Order> orders = page.getContent();
        model.addAttribute("orders", orders);
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("sortField", sort);
        model.addAttribute("direction", dir);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("reverseDirection", dir.equals("asc") ? "desc" : "asc");
        return "userPage";
    }

    @GetMapping("/newOrder")
    public String newOrder() {
        return "newOrder";
    }

    @PatchMapping("/makeOrder/{id}")
    public String makeOrder(@PathVariable("id") Long id) {
        orderService.addOrder(id);
        return "redirect:/userPage";
    }

    @PostMapping("/requestOrder/{bookId}")
    public String requestOrder(OrderDTO orderDTO,
                               @PathVariable("bookId") Long bookId,
                               Authentication authentication) {
        User currentUser = (User) authentication.getPrincipal();
        orderService.requestOrder(currentUser, bookId, orderDTO);
        return "redirect:/userPage";
    }

    @GetMapping("/showAllOrders")
    public String showAllOrders(Model model) {
        model.addAttribute("showAllOrders", orderService.showAllOrders());
        return "redirect:/userPage";
    }
}
