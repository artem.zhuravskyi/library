package com.example.demo.model;

import com.example.demo.dto.OrderDTO;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;


@Table(name = "orders")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Book book;
    @ManyToOne
    private User user;

    private OrderDTO.ReadingPlace readingPlace;

    private Status status;

    public enum Status {
        REQUESTED,
        ORDERED
    }
    private long fine;

    public enum State {
        EXPIRED,
        ACTIVE
    }

    private State state = State.ACTIVE;

    private LocalDate orderDate;
    private LocalDate expireDate;



}
