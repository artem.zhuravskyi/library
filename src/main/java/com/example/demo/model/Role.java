package com.example.demo.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_LIBRARIAN;

    @Override
    public String toString() {
        return this.name().replace("ROLE_", "");
    }
}

