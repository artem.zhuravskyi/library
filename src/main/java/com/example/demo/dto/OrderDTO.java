package com.example.demo.dto;

import com.example.demo.model.Order;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class OrderDTO {
    private ReadingPlace readingPlace;

    public enum ReadingPlace {
        AT_LIBRARY,
        AT_HOME;
    }
}
