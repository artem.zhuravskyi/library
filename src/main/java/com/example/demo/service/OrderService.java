package com.example.demo.service;

import com.example.demo.dao.BookRepository;
import com.example.demo.dao.OrderRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.dto.OrderDTO;
import com.example.demo.model.Book;
import com.example.demo.model.Order;
import com.example.demo.model.User;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static com.example.demo.model.Order.Status.ORDERED;
import static com.example.demo.model.Order.Status.REQUESTED;

@AllArgsConstructor
@Service

public class OrderService {

    private OrderRepository orderRepository;
    private UserRepository userRepository;
    private BookRepository bookRepository;
    public final static int PAGE_SIZE = 10;

    public List<Order> orderInfo(User currentUser) {

        switch (currentUser.getRole()) {
            case ROLE_LIBRARIAN:
                return orderRepository.findOrderByStatus(REQUESTED);
            case ROLE_USER:
            default:
                return orderRepository.findOrderByUserAndStatus(currentUser, ORDERED);

        }
    }
//    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = NoSuchElementException.class)
    public void requestOrder(User user,
                         Long bookId,
                         OrderDTO orderDTO) {

        Order order = Order.builder()
                .book(bookRepository.findById(bookId).get())
                .user(user)
                .readingPlace(orderDTO.getReadingPlace())
                .orderDate(LocalDate.now())
                .expireDate(LocalDate.now().plusDays(1))
                .status(REQUESTED)
                .build();
        orderRepository.save(order);
    }
//    @Transactional (propagation = Propagation.REQUIRED, rollbackFor = NoSuchElementException.class)
    public void addOrder(Long id) {

        Order order = orderRepository.findById(id).get();
        Book book = bookRepository.findById(order.getBook().getId()).get();
        order.setStatus(ORDERED);
        book.setQuantity(book.getQuantity() - 1);
        orderRepository.save(order);
        if (book.getQuantity() == 0) {
            orderRepository.deleteAllByBookAndStatus(book, REQUESTED);
        }

        bookRepository.save(book);



    }

    public List<User> showAllOrders() {
        return userRepository.findAll();
    }

//    @Scheduled(fixedDelay = 1000)
//    public void scheduleFixedDelayTask() {
//        List<Order> orderExpDate = orderRepository.findByStateIs(Order.State.ACTIVE);
//        orderExpDate.stream().filter(order -> LocalDate.now().isAfter(order.getExpireDate()))
//                .forEach(order -> {
//                            order.setState(Order.State.EXPIRED);
//                            order.setFine(order.getFine() + 50);
//                        }
//                );
//    }

    public Page<Order> getPaginated(int pageNo, String sortField, String sortDirection){
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ?
                Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, PAGE_SIZE, sort);
        return orderRepository.findAll(pageable);
    }

}
