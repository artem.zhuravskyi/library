package com.example.demo.service;

import com.example.demo.dao.BookRepository;
import com.example.demo.dao.OrderRepository;
import com.example.demo.dto.BookDTO;
import com.example.demo.model.Book;
import com.example.demo.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class BookService {

    private final BookRepository bookRepository;
    private final OrderRepository orderRepository;

    public List<Book> searchByTitle(String searchContent) {
        return bookRepository.findByTitleContainsIgnoreCase(searchContent);
    }

    public List<Book> searchByAuthor(String searchContent) {
        return bookRepository.findByAuthorContainsIgnoreCase(searchContent);
    }

    public void addBook(BookDTO bookDTO) {
        Optional<Book> book = bookRepository
                .findBookByAuthorAndTitleAndPublisher(bookDTO.getAuthor(), bookDTO.getTitle(), bookDTO.getPublisher());
        if (book.isPresent()) {
            book.get().setQuantity(book.get().getQuantity() + 1);
            bookRepository.save(book.get());
        } else {
            Book newBook = Book.builder()
                    .author(bookDTO.getAuthor())
                    .title(bookDTO.getTitle())
                    .dateOfPublish(LocalDate.now())
                    .publisher(bookDTO.getPublisher())
                    .quantity(1)
                    .build();
            bookRepository.save(newBook);
        }
    }


    public void returnBook(Long id) {
        orderRepository.delete(orderRepository.findById(id).get());
        Book book = bookRepository.findById(orderRepository.findById(id).get().getId()).get();
        book.setQuantity(book.getQuantity() + 1);
        bookRepository.save(book);
    }

    public void deleteBook(Long id) {
        bookRepository.delete(bookRepository.findById(id).get());
    }

    public Book findProperBook(Long id) {
        return bookRepository.findById(id).get();
    }

    public void editBook(Long id, BookDTO bookDTO) {
        Book book = bookRepository.findById(id).get();
        book.setAuthor(bookDTO.getAuthor());
        book.setTitle(bookDTO.getTitle());
        book.setPublisher(bookDTO.getPublisher());
        book.setQuantity(bookDTO.getQuantity());
        bookRepository.save(book);
    }
}
